# -*- coding: utf-8 -*-

from datetime import timedelta
from odoo import models, fields, api, exceptions


class Room(models.Model):
    _inherits = {'product.product': 'product_id' }
    _name = 'lleialges.room'
    
    # not cascading?
    product_id = fields.Many2one('product.product', ondelete='cascade', string="Product ID", required=True)
    
    description = fields.Text()
    capacity = fields.Integer(string="People permitted", required=True)
    activity_ids = fields.One2many('lleialges.activity', 'room_id', string="Activities")



class ActivityTag(models.Model):
    _name = 'lleialges.activity.tag'
    
    name = fields.Char(string="Name", required=True)
    description = fields.Text()
    color = fields.Integer(string='Color Index')
    activity_ids = fields.Many2many('lleialges.activity', string="Activities", readonly=True)



class Activity(models.Model):
    _name = 'lleialges.activity'

    name = fields.Char(required=True, string="Activity name")
    description = fields.Text()
    responsible_id = fields.Many2one('res.partner', string="Responsible", required=True)
    room_id = fields.Many2one('lleialges.room', ondelete='cascade', string="Room", required=True)
    start_date = fields.Datetime()
    end_date = fields.Datetime()
    calendar_name = fields.Char(compute='_get_calendar_name')
    duration = fields.Float(digits=(6, 2), compute='_get_duration')

    attendants = fields.Integer(string="Max people assisting")
    activity_tag_ids = fields.Many2many('lleialges.activity.tag', string="Tags")


    @api.constrains('room_id', 'start_date', 'end_date', 'attendants')
    def _validation(self):
        if (self.attendants > self.room_id.capacity):
            raise exceptions.ValidationError("'%s' too small. max people: %s" % (self.room_id.name, self.room_id.capacity))
        
        if (self.end_date < self.start_date):
            raise exceptions.ValidationError("Bad dates")
            
        recordSet = self.search([('room_id', '=', self.room_id.id),
                                    ('start_date', '<', self.end_date),
                                    ('end_date', '>', self.start_date)])
        if len(recordSet) > 1:
            raise exceptions.ValidationError("%s already occupied!!" % self.room_id.name)
    
      
    @api.depends('name', 'responsible_id', 'duration')
    def _get_calendar_name(self):
        for r in self:
            r.calendar_name = "%s: %s: %s" % (r.name, r.responsible_id.name, r.duration)

    @api.depends('start_date', 'end_date')
    def _get_duration(self):
        for r in self:
            start_date = fields.Datetime.from_string(r.start_date)
            end_date = fields.Datetime.from_string(r.end_date)
            parts = str(end_date - start_date).split(":")
            hours = parts[0] if len(parts) > 0 else 0.0
            minutes = parts[1] if len(parts) > 1 else 0.0
            seconds = parts[2] if len(parts) > 2 else 0.0
            r.duration = float(hours) + (float(minutes) / 60.0) + (float(seconds) / pow(60.0, 2))            

    @api.multi
    def create_invoice(self):
        # This will make sure that you have one record instead of multiple (singleton)
        self.ensure_one()
        client_id = self.responsible_id.id
 
        invoice_vals = {
            'partner_id': client_id,
            'date_invoice': fields.Datetime.now(),
            #'validity_date': datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S'),
            #'order_line': item_lines,
        }
        account_invoice = self.env['account.invoice'].create(invoice_vals)
 
        #?? http://findnerd.com/list/view/How-to-calculate-fields-with-total-in-account-invoice-line-in-Odoo-9/19879/
        recordSet = self.search([('responsible_id','=', client_id)])
        
        for activity in recordSet:
            line_vals = {
                'product_id': activity.room_id.product_id.id,
                'name': activity.room_id.product_id.name,
                'description': activity.name, # ??
                'quantity': activity.duration,
                'price_unit': activity.room_id.product_id.list_price,
                'invoice_id' : account_invoice.id,
                'account_id': 1, #??
                'invoice_line_tax_id': (6, 0, [51])     # Tax id = 51 ??
            }
            self.env['account.invoice.line'].create(line_vals)
            

        invoice_form = self.env.ref('account.invoice_form', False)
        return {
            'name': 'Invoice',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'account.invoice',
            'views': [(invoice_form.id, 'form')],
            'view_id': invoice_form.id,
            'res_id': account_invoice.id,
            'target': 'current',
            'flags': {'form': {'action_buttons': True, 'options': {'mode': 'edit'}}},
            'context': dict(
                    default_partner_id=client_id,
                ),
        }
